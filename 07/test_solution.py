import pytest

from solution import unique_outer_bags, number_bags_in_color

rules = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""

rules_too = """shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."""


def test_unique_outer_bags():
    assert unique_outer_bags(rules, "shiny gold") == 4


@pytest.mark.parametrize(
    "color, rules, bags_inside",
    [("shiny gold", rules, 32), ("shiny gold", rules_too, 126),],
)
def test_number_bags_in_color(color, rules, bags_inside):
    assert number_bags_in_color(rules, color) == bags_inside
