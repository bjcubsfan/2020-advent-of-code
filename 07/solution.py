#!/usr/bin/env python

from pprint import pprint


def direct_holding_colors(rules, color_to_watch):
    direct_outer_bags = set()
    for rule in rules.split("\n"):
        rule = rule.strip()
        if rule == "":
            continue
        # print(f"\n\nrule ({color_to_watch}): {rule}")
        rule = rule.split()
        outer_bag_color = " ".join(rule[0:2])
        # print(f"outer bag color: {outer_bag_color}")
        try:
            if rule[4] == "no":
                colors = []
            else:
                # Count other bags
                other = True
                index = 5
                colors = []
                while other:
                    new_color = " ".join(rule[index : index + 2])
                    colors.append(new_color)
                    if rule[index + 2][-1] == ".":
                        other = False
                    else:
                        # more colors
                        index = index + 4
        except IndexError:
            print("IndexError on {rule}")
        # print(f"colors: {colors}")
        if color_to_watch in colors:
            direct_outer_bags.add(outer_bag_color)
        # print(f"for '{color_to_watch}' returning {direct_outer_bags}")
    return direct_outer_bags


def unique_outer_bags(rules, color_to_watch):
    """Find unique outer bags that could have a color in there somewhere.

    "pale purple" holds "shiny gold"
    "vibrant coral" holds "shiny gold"
    "dotted fuchsia" holds "shiny gold"
    "dotted magenta" holds "pale purple"
    "shiny yellow" holds "vibrant coral"
    "plaid green" contains "dotted fuchsia"
    "vibrant bronze" contains "dotted fuchsia"
    "clear maroon" contains "dotted fuchsia"
    "wavy bronze" contains "dotted fuchsia"
    "vibrant teal" contains "dotted fuchsia"
    ..... there are more
    """
    bags_that_hold_somewhere = set()
    direct_outer_bags = direct_holding_colors(rules, color_to_watch)
    bags_that_hold_somewhere = direct_outer_bags | bags_that_hold_somewhere
    # print(f"after direct: {bags_that_hold_somewhere}")
    set_changed = True
    while set_changed:
        # print(f"testing set: {bags_that_hold_somewhere}")
        new_bags_somewhere = set() | bags_that_hold_somewhere
        for color in bags_that_hold_somewhere:
            holds_this_color = direct_holding_colors(rules, color)
            new_bags_somewhere = new_bags_somewhere | holds_this_color
            # print(f"new bags somewhere: {new_bags_somewhere}\n")
        if new_bags_somewhere == bags_that_hold_somewhere:
            set_changed = False
        else:
            bags_that_hold_somewhere = new_bags_somewhere
        # print(f"after {color}: {bags_that_hold_somewhere}")
    return len(bags_that_hold_somewhere)


total_number_of_bags = 0


def count_bags(color, parsed_rules):
    global total_number_of_bags
    bags_in_color = parsed_rules[color]
    # print(f"Looking at the {color} bag")
    for bag_in_color in bags_in_color:
        total_number_of_bags += 1
        # print(f"Adding {bag_in_color} to {color}, now {total_number_of_bags}")
        if parsed_rules[bag_in_color] == []:
            # print(f"Done with the {color} bag")
            continue
        else:
            count_bags(bag_in_color, parsed_rules)


def number_bags_in_color(rules, color):
    global total_number_of_bags
    total_number_of_bags = 0
    color_rules = dict()
    # print("Analyzing rules")
    for rule in rules.split("\n"):
        # print(f"Analyzing rule >>{rule}<<")
        rule = rule.strip()
        if rule == "":
            continue
        rule = rule.split()
        outer_bag_color = " ".join(rule[0:2])
        # print(f"looking at outer color: {outer_bag_color}")
        try:
            if rule[4] == "no":
                bags_inside = []
            else:
                # Count other bags
                other = True
                index = 4
                bags_inside = []
                while other:
                    number_of_color = int(rule[index])
                    new_color = " ".join(rule[index + 1 : index + 3])
                    bags_inside.extend(number_of_color * [new_color])
                    #  print(f"just added {number_of_color} of {new_color} to {outer_bag_color}")
                    if rule[index + 3][-1] == ".":
                        other = False
                    else:
                        # more colors
                        index = index + 4
        except IndexError:
            print("IndexError on {rule}")
        color_rules[outer_bag_color] = bags_inside
        # print(f"final is {outer_bag_color}: {color_rules[outer_bag_color]}")
        # print()
    # print("Here are the color rules:")
    # pprint(color_rules)
    # print()
    total_bags = 0
    count_bags(color, color_rules)
    # print(total_number_of_bags)
    # print()
    return total_number_of_bags


def main():
    with open("input") as input_data:
        input_data = input_data.read()
        uob = unique_outer_bags(input_data, "shiny gold")
        nbi = number_bags_in_color(input_data, "shiny gold")
        print(f"unique outer bags: {uob}")
        print(f"number of bags in color: {nbi}")


if __name__ == "__main__":
    main()
