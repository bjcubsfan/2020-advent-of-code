import pytest

from solution import num_valid, corrected_num_valid

input_data = """1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc"""


def test_num_valid():
    policy_and_passwords = input_data.split("\n")
    assert 2 == num_valid(policy_and_passwords)


def test_corrected_num_valid():
    policy_and_passwords = input_data.split("\n")
    assert 1 == corrected_num_valid(policy_and_passwords)
