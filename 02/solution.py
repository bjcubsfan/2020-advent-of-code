#!/usr/bin/env python


def num_valid(policy_and_passwords):
    valid_passwords = 0
    for policy_and_password in policy_and_passwords:
        policy, password = policy_and_password.split(":")
        repeats, letter = policy.split()
        min_repeat, max_repeat = repeats.split("-")
        min_repeat = int(min_repeat)
        max_repeat = int(max_repeat)
        count = 0
        for character in password:
            if character == letter:
                count += 1
        if min_repeat <= count <= max_repeat:
            valid_passwords += 1
    return valid_passwords


def corrected_num_valid(policy_and_passwords):
    valid_passwords = 0
    for policy_and_password in policy_and_passwords:
        policy, password = policy_and_password.split(":")
        password = password.strip()
        indexes, letter = policy.split()
        first_index, second_index = indexes.split("-")
        first_index = int(first_index) - 1  # correct to python 0 indexing
        second_index = int(second_index) - 1  # correct to python 0 indexing
        valid = False
        if (password[first_index] == letter) != (password[second_index] == letter):
            valid_passwords += 1
            valid = True
        # print(f"letter={letter}, 1i={first_index}, "
        #     f"2i={second_index}, password='{password}', valid={valid}")
    return valid_passwords


def main():
    with open("input") as input_data:
        policy_and_passwords = input_data.read().strip().split("\n")
        print(num_valid(policy_and_passwords))
        print(corrected_num_valid(policy_and_passwords))


if __name__ == "__main__":
    main()
