#!/usr/bin/env python

from collections import Counter


def value_checks_valid(split_passport):
    """Check the values on all the fields"""

    # print("**** START EXTENDED VALIDATION ****")
    byr = int(split_passport["byr"])
    # print(f"byr {byr}")
    if byr < 1920 or byr > 2002:
        return False
    iyr = int(split_passport["iyr"])
    # print(f"iyr {iyr}")
    if iyr < 2010 or iyr > 2020:
        return False
    eyr = int(split_passport["eyr"])
    # print(f"eyr {eyr}")
    if eyr < 2020 or eyr > 2030:
        return False
    hgt = split_passport["hgt"]
    # print(f"hgt {hgt}")
    number, units = hgt[:-2], hgt[-2:]
    if not (units == "cm" or units == "in"):
        return False
    number = int(number)
    if units == "cm":
        if number <= 150 or number > 193:
            return False
    else:
        # units == in
        if number <= 59 or number > 76:
            return False
    hcl = split_passport["hcl"]
    # print(f"hcl {hcl}")
    if not len(hcl) == 7:
        return False
    if not hcl[0] == "#":
        return False
    for digit in hcl[1:]:
        if digit not in [
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
        ]:
            return False
    ecl = split_passport["ecl"]
    # print(f"ecl {ecl}")
    if ecl not in [
        "amb",
        "blu",
        "brn",
        "gry",
        "grn",
        "hzl",
        "oth",
    ]:
        return False
    pid = split_passport["pid"]
    # print(f"pid {pid}")
    if len(pid) != 9:
        return False
    pid = int(pid)
    # print("all passed")
    return True


def passport_is_valid(passport, perform_value_checks):
    """determine if this passport is valid.

    Possible valid fields are:

    byr (Birth Year)
    iyr (Issue Year)
    eyr (Expiration Year)
    hgt (Height)
    hcl (Hair Color)
    ecl (Eye Color)
    pid (Passport ID)
    cid (Country ID)

    cid can be missing
    """
    field_count = Counter(
        {
            "byr": 0,
            "iyr": 0,
            "eyr": 0,
            "hgt": 0,
            "hcl": 0,
            "ecl": 0,
            "pid": 0,
            "cid": 0,
        }
    )
    fields = passport.split()
    split_passport = dict()
    for field in fields:
        key, value = field.split(":")
        key = key.strip()
        value = value.strip()
        split_passport[key] = value
        field_count[key] += 1
    fields_correct = True
    for key, occurances in field_count.items():
        if occurances != 1:
            if key == "cid" and occurances == 0:
                continue
            fields_correct = False
    if not fields_correct or not perform_value_checks:
        return fields_correct
    else:
        return value_checks_valid(split_passport)


def number_valid_passports(input_data, perform_value_checks):
    """Determine the number of valid passports."""

    input_data = input_data.strip()
    input_data = input_data.split(
        """

"""
    )
    valid_passports = 0
    for passport in input_data:
        if passport_is_valid(passport, perform_value_checks):
            valid_passports += 1
    return valid_passports


def main():
    with open("input") as input_data:
        input_data = input_data.read()
        print(f"first validation: {number_valid_passports(input_data, False)}")
        print(f"extended validat: {number_valid_passports(input_data, True)}")


if __name__ == "__main__":
    main()
