import pytest

from solution import mult_two_that_sum_to_2020, mult_three_that_sum_to_2020

INPUT = """1721
979
366
299
675
1456"""


def test_mult_two_that_sum_to_2020():
    numbers = INPUT.split()
    assert mult_two_that_sum_to_2020(numbers) == 514579


def test_mult_three_that_sum_to_2020():
    numbers = INPUT.split()
    assert mult_three_that_sum_to_2020(numbers) == 241861950
