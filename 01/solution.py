#!/usr/bin/env python

from itertools import combinations


def mult_two_that_sum_to_2020(numbers):
    for first, second in combinations(numbers, 2):
        first = int(first)
        second = int(second)
        if first + second == 2020:
            return first * second


def mult_three_that_sum_to_2020(numbers):
    for first, second, third in combinations(numbers, 3):
        first = int(first)
        second = int(second)
        third = int(third)
        if first + second + third == 2020:
            return first * second * third


def main():
    with open("input") as input_data:
        numbers = []
        for line in input_data:
            numbers.append(int(line))
        print(f"2 multiplied = {mult_two_that_sum_to_2020(numbers)}")
        print(f"3 multiplied = {mult_three_that_sum_to_2020(numbers)}")


if __name__ == "__main__":
    main()
