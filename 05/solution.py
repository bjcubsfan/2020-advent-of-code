#!/usr/bin/env python


def interpret_ticket(ticket):
    ticket = ticket.strip()
    row_ones_zeros = []
    for row_digit in ticket[0:7]:
        if row_digit == "F":
            digit = "0"
        elif row_digit == "B":
            digit = "1"
        row_ones_zeros.append(digit)
    row = int("".join(row_ones_zeros), 2)
    seat_ones_zeros = []
    for seat_digit in ticket[7:]:
        if seat_digit == "L":
            digit = "0"
        elif seat_digit == "R":
            digit = "1"
        seat_ones_zeros.append(digit)
    seat = int("".join(seat_ones_zeros), 2)
    ticket_id = row * 8 + seat
    return row, seat, ticket_id


def main():
    with open("input") as input_data:
        ticket_ids = []
        for line in input_data:
            row, seat, ticket_id = interpret_ticket(line)
            ticket_ids.append(ticket_id)
        print(max(ticket_ids))
        possible_tickets = list(range(min(ticket_ids), max(ticket_ids)))
        for ticket_id in ticket_ids:
            if ticket_id in possible_tickets:
                possible_tickets.pop(possible_tickets.index(ticket_id))
        print(possible_tickets)


if __name__ == "__main__":
    main()
