import pytest

from solution import interpret_ticket


@pytest.mark.parametrize(
    "ticket, row, seat, ticket_id",
    [
        ("FBFBBFFRLR", 44, 5, 357),
        ("BFFFBBFRRR", 70, 7, 567),
        ("FFFBBBFRRR", 14, 7, 119),
        ("BBFFBBFRLL", 102, 4, 820),
    ],
)
def test_interpret_ticket(ticket, row, seat, ticket_id):
    calc_row, calc_seat, calc_ticket_id = interpret_ticket(ticket)
    assert calc_row == row
    assert calc_seat == seat
    assert calc_ticket_id == ticket_id
