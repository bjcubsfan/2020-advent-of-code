#!/usr/bin/env python


def parse_instruction(instruction_line):
    instruction, sign, argument = instruction_line.split()
    argument = int(argument)
    if sign == "-":
        argument = argument * -1
    return instruction, sign, argument


def acc_value_at_repeat(instructions):
    continue_processing = True
    accumulator = 0
    instructions_executed = list()
    index = 0
    terminated_index = len(instructions)
    terminated = False
    while continue_processing:
        if index in instructions_executed:
            continue_processing = False
            continue
        elif terminated_index == index:
            continue_processing = False
            terminated = True
            continue
        instruction_line = instructions[index]
        instruction, sign, argument = parse_instruction(instruction_line)
        instructions_executed.append(index)
        if instruction == "nop":
            index += 1
        elif instruction == "acc":
            index += 1
            accumulator += argument
        elif instruction == "jmp":
            index += argument
    return terminated, accumulator


def try_swap(index_to_try, instruction, instructions):
    if instruction == "nop":
        new_instruction = "jmp"
    else:
        new_instruction = "nop"
    try_these_instructions = instructions[:]
    original_line = try_these_instructions[index_to_try]
    new_line = new_instruction + original_line[3:]
    try_these_instructions[index_to_try] = new_line
    # print(f"trying these:\n{try_these_instructions}")
    terminated, accumulator = acc_value_at_repeat(try_these_instructions)
    return terminated, accumulator


def change_to_terminate(instructions):
    for index_to_try in range(len(instructions)):
        instruction_line = instructions[index_to_try]
        instruction, sign, argument = parse_instruction(instruction_line)
        if instruction == "acc":
            continue
        else:
            terminated, accumulator = try_swap(index_to_try, instruction, instructions)
        if terminated:
            return accumulator
        # print(f"changing {index_to_try} did not help.")
    return -1


def main():
    with open("input") as input_file:
        input_data = input_file.read()
        instructions = input_data.strip().split("\n")
        print(f"acc value at repeat: {acc_value_at_repeat(instructions)}")
        print(f"acc value at terminate: {change_to_terminate(instructions)}")


if __name__ == "__main__":
    main()
