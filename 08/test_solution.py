import pytest

from solution import acc_value_at_repeat, change_to_terminate

instructions = """nop + 0
acc + 1
jmp + 4
acc + 3
jmp - 3
acc - 99
acc + 1
jmp - 4
acc + 6"""

instructions = instructions.strip().split("\n")


def test_acc_value_at_repeat():
    terminated, accumulator = acc_value_at_repeat(instructions)
    assert accumulator == 5


def test_change_to_terminate():
    assert change_to_terminate(instructions) == 8
