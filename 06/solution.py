#!/usr/bin/env python

from collections import Counter


def count_answers(input_data):
    group_counts = []
    total = 0
    for group in input_data.split("\n\n"):
        group = group.strip()
        # print(f"group is: >>{group}<<")
        answers = Counter()
        for line in group:
            line = line.strip()
            for answer in line:
                answers[answer] += 1
        total += len(answers)
        # print(f"counter is {answers} with len {len(answers)}")
        group_counts.append(answers)
    return group_counts, total


def count_everyone_answers(input_data):
    group_counts = []
    total = 0
    for group in input_data.split("\n\n"):
        group = group.strip()
        # print(f"group is: >>{group}<<")
        answers = Counter()
        people_in_group = 0
        group_count = 0
        for line in group.split("\n"):
            if line == "/n" or line == "":
                continue
            # print(f"line in group {line}")
            line = line.strip()
            people_in_group += 1
            for answer in line:
                answers[answer] += 1
        # print(f"Answers: {answers}. . . people: {people_in_group}")
        for answer, num in answers.items():
            if num == people_in_group:
                group_count += 1
        total += group_count
        #  print(f"group_count is {group_count}, counter is {answers} with {people_in_group} people")
        group_counts.append(answers)
    return group_counts, total


def main():
    with open("input") as input_data:
        input_data = input_data.read().strip()
        answers, total = count_answers(input_data)
        print(total)
        answers, total = count_everyone_answers(input_data)
        print(total)


if __name__ == "__main__":
    main()
