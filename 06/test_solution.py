import pytest

from solution import count_answers, count_everyone_answers

input_data = """abc

a
b
c

ab
ac

a
a
a
a

b"""


def test_count_answers():
    answers, total = count_answers(input_data)
    assert total == 11


def test_count_everyone_answers():
    answers, total = count_everyone_answers(input_data)
    assert total == 6
