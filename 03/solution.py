#!/usr/bin/env python


def how_many_trees(input_data, right, down):
    input_data = input_data.strip().split("\n")
    current_position = 0
    current_row_index = 0
    number_of_trees = 0
    while current_row_index < len(input_data):
        # print(f"{current_position} - {row_of_trees[current_position]}")
        row_of_trees = input_data[current_row_index]
        if row_of_trees[current_position] == "#":
            number_of_trees += 1
        current_position = (current_position + right) % len(row_of_trees)
        current_row_index += down
    return number_of_trees


def main():
    with open("input") as input_data:
        input_data = input_data.read()
        outputs = [
            how_many_trees(input_data, 1, 1),
            how_many_trees(input_data, 3, 1),
            how_many_trees(input_data, 5, 1),
            how_many_trees(input_data, 7, 1),
            how_many_trees(input_data, 1, 2),
        ]
        result = 1
        for number in outputs:
            result = result * number
        print(result)


if __name__ == "__main__":
    main()
