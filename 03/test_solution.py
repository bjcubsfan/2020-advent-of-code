import pytest

from solution import how_many_trees

tree_map = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"""


@pytest.mark.parametrize(
    "right, down, number_of_trees",
    [(1, 1, 2), (3, 1, 7), (5, 1, 3), (7, 1, 4), (1, 2, 2),],
)
def test_how_many_trees(right, down, number_of_trees):
    assert how_many_trees(tree_map, right, down) == number_of_trees
